import inverted_indexing

from flask import Flask, request, render_template
app = Flask(__name__)


@app.route("/")
def first_page():
    return render_template("index.html")


@app.route("/result", methods=["GET","POST"])
def result_page():
    doc_data = request.form["doc_data"]
    inverted_index = inverted_indexing.create_inverted_index()
    label, confidence = inverted_indexing.get_doc_label(inverted_index, doc_data.strip())
    return str(label+", "+str(confidence))


if __name__ == "__main__":
    app.run(threaded=True)